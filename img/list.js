var walk = require('walk'),
    path = require('path'),
    fs = require('fs');
var files = [];

// Walker options
var walker  = walk.walk('.', { followLinks: false });

walker.on('file', function(root, stat, next) {
    // Add this file to the list of files
    if (stat.type === 'file' && ['.jpg', '.jpeg', '.png'].indexOf(path.extname(stat.name)) > -1) {
        files.push(root + '/' + stat.name);
    }
    next();
});

walker.on('end', function() {
    console.log(files);
    fs.writeFile('images.json', JSON.stringify(files), function (err) {
        if (err) {
            throw err;
        } else {
            console.log('It\'s saved!');
        }
    });
});