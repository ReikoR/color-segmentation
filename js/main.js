var hsvLUT = new Float32Array(256 * 256 * 256 * 3);

$(window).load(function () {
    var canvas = $('#source')[0],
        outputCanvas = $('#output')[0],
        context = canvas.getContext('2d'),
        $sourceImage = $('#source-image');

    var canSegment = true;;

    var r, g, b,
        ri, gi,
        hsv;

    /*var lutStart = Date.now();
    for (r = 0; r < 256; r++) {
        ri = r * 256 * 256;
        for (g = 0; g < 256; g++) {
            gi = g * 256;
            for (b = 0; b < 256; b++) {
                hsv = calcRGBtoHSV(r, g, b);
                hsvLUT.set(hsv, 3 * (ri + gi + b));
            }
        }
    }
    console.log('LUT', Date.now() - lutStart);*/

    function startProcessing ($source) {
        console.log($source.width(), $source.height());

        canvas.width = $source.width();
        canvas.height = $source.height();
        context.drawImage($source[0], 0, 0);

        var imageData = context.getImageData(0, 0, canvas.width, canvas.height);

        var i,
            pixelData,
            hsv;

        var lutStart = Date.now();
        for (i = 0; i < imageData.data.length; i += 4) {
            pixelData = imageData.data.subarray(i, i+ 3);
            hsv = calcRGBcolorToHSV(pixelData);
            hsvLUT.set(hsv, 3 * (pixelData[0] * 65536 + pixelData[1] * 256 + pixelData[2]));
        }
        console.log('LUT', Date.now() - lutStart);

        $(document).on('keydown', function (e) {
            e.stopPropagation();
            //e.preventDefault();
            console.log(e.which);

            if (e.which === 83) { //s
                $('#snapshot').attr('src', $('#output')[0].toDataURL());
            }

        });

        segment(canvas, outputCanvas, function () {
            canSegment = true;
        });
    }

    function loadImage(imageName) {
        $sourceImage.attr('src', imageName);
        $sourceImage.one('load', function() {
            console.log('load');

            $('#snapshots').empty();
            $('#log').empty();
            startProcessing($sourceImage);

            //context.drawImage($sourceImage[0], 0, 0);

            //segment(canvas, outputCanvas);
        }).each(function() {
            if(this.complete) $(this).load();
        });
    }

    /*$sourceImage.one('load', function() {
        startProcessing($sourceImage);

    }).each(function() {
        if(this.complete) $(this).load();
    });*/

    $.getJSON('img/images.json', function (data) {
        console.log(data);

        var $images = $('#images');

        data.forEach(function (imageSrc, index) {
            $images.append('<img src="' + 'img/' + data[index] + '"/>');
        });

        $images.on('click', 'img', function () {
            console.log('click');
            if (canSegment) {
                canSegment = false;
                loadImage($(this).attr('src'));
            }
        });
    });

});

function segment(sourceCanvas, resultCanvas, callback) {
    console.log('segment');
    resultCanvas.width = sourceCanvas.width;
    resultCanvas.height = sourceCanvas.height;

    var $log = $('#log');

    var stepDelay = 0;

    var createRegions = true,
        growRegions = true,
        mergeByColor = true,
        mergeBySize = true;

    var colorMergeThreshold = 0.2,
        sizeMergeThreshold = sourceCanvas.width * sourceCanvas.height / 200;

    var sourceContext = sourceCanvas.getContext('2d'),
        resultContext = resultCanvas.getContext('2d'),
        sourceData = sourceContext.getImageData(0, 0, sourceCanvas.width, sourceCanvas.height),
        resultData = resultContext.getImageData(0, 0, sourceCanvas.width, sourceCanvas.height),
        x, y, i,
        gImage = calcFuzzyEntropyImage(sourceData.data, sourceCanvas.width),
        gImageStats = valuesStats(gImage),
        dImage = calcSmoothnessImage(sourceData.data, sourceCanvas.width),
        dImageStats = valuesStats(dImage),
        regionMapIndex,
        regionStats = [];

    var regionMap = new Int32Array(sourceCanvas.width * sourceCanvas.height);

    var prevRegionNumber = 0;

    $(resultCanvas).click(function (e) {
        var pos = findPos(this);
        var x = e.pageX - pos.x;
        var y = e.pageY - pos.y;

        var regionMapIndex = y * resultCanvas.width + x;
        var regionNumber = regionMap[regionMapIndex];
        var distance = 0;

        if (regionNumber > 0 && prevRegionNumber > 0) {
            distance = hsvDistance(
                calcRGBcolorToHSV(regionStats[regionNumber - 1].mean),
                calcRGBcolorToHSV(regionStats[prevRegionNumber - 1].mean)
            );

            console.log(regionNumber, prevRegionNumber, distance);
        }

        prevRegionNumber = regionNumber;
    });

    function draw() {
        var regionStat;

        if (createRegions) {
            for (y = 0; y < sourceCanvas.height; y++) {
                for (x = 0; x < sourceCanvas.width; x++) {
                    i = getPixelCoordinate(sourceCanvas.width, x, y);
                    regionMapIndex = y * sourceCanvas.width + x;


                    regionStat = regionStats[regionMap[regionMapIndex] - 1];

                    if (regionStat) {
                        resultData.data.set(regionStat.mean, i);
                        resultData.data.set([255], i + 3);
                    } else {
                        resultData.data.set([0, 0, 0, 0], i);
                    }
                }
            }
        }

        resultContext.putImageData(resultData, 0, 0);
    }

    function takeSnapShot() {
        $('#snapshots').append('<img src="' + resultCanvas.toDataURL() + '"/>');
    }

    for (y = 0; y < sourceCanvas.height; y++) {
        for (x = 0; x < sourceCanvas.width; x++) {
            i = getPixelCoordinate(sourceCanvas.width, x, y);
            regionMapIndex = y * sourceCanvas.width + x;
            if (isSeedPixel(gImage, gImageStats, dImage, dImageStats, sourceCanvas.width, x, y)) {
                regionMap[regionMapIndex] = -1;
                resultData.data.set([0, 255, 0, 255], i);
            } else {
                regionMap[regionMapIndex] = 0;
                resultData.data.set([0, 0, 0, 255], i);
            }
        }
    }
    $log.append('<div>Creating seeds done</div>');
    resultContext.putImageData(resultData, 0, 0);
    takeSnapShot();

    setTimeout(createRegionsStep, 1000);

    var adjacentCoords,
        coord,
        c,
        regionNumber;

    var queue = [];

    //seed regions
    function createRegionsStep() {
        if (!createRegions) {
            return
        }

        for (y = 0; y < sourceCanvas.height; y++) {
            for (x = 0; x < sourceCanvas.width; x++) {
                i = getPixelCoordinate(sourceCanvas.width, x, y);
                regionMapIndex = y * sourceCanvas.width + x;

                if (regionMap[regionMapIndex] === -1) {
                    regionStats.push({
                        mean: sourceData.data.subarray(i, i + 3),
                        count: 1
                    });
                    regionNumber = regionStats.length;
                    regionMap[regionMapIndex] = regionNumber;

                    queue = queue.concat(calcAdjacentCoordinates(sourceCanvas.width, sourceCanvas.height, x, y));
                    while (queue.length > 0) {
                        coord = queue.shift();
                        c = getPixelCoordinate(sourceCanvas.width, coord.x, coord.y);
                        regionMapIndex = coord.y * sourceCanvas.width + coord.x;
                        if (regionMap[regionMapIndex] === -1) {
                            regionStats[regionNumber - 1].mean = runningColorMean(
                                regionStats[regionNumber - 1].mean,
                                regionStats[regionNumber - 1].count,
                                sourceData.data.subarray(c, c + 3)
                            );
                            regionStats[regionNumber - 1].count++;
                            regionMap[regionMapIndex] = regionNumber;
                            queue = queue.concat(calcAdjacentCoordinates(sourceCanvas.width, sourceCanvas.height, coord.x, coord.y));
                        }
                    }
                }
            }
        }

        $log.append('<div>Creating regions done</div>');
        draw();
        takeSnapShot();

        setTimeout(startGrowingRegions, 1000);
    }

    function closestRegion(x, y, adjacentCoords) {
        var closestRegion = 0,
            smallestDistance = 10000,
            smallestDistanceStats = {count: 0, mean: [0, 0, 0]},
            i,
            coord,
            coords = [],
            closestCoord,
            regionMapIndex,
            pixelCoord = getPixelCoordinate(sourceCanvas.width, x, y),
            stats = [],
            distance,
            regionNumber,
            regionNumberSum = 0,
            unLabeled = [],
            closestIndex;

        for (i = 0; i < adjacentCoords.length; i++) {
            coord = adjacentCoords[i];
            regionMapIndex = coord.y * sourceCanvas.width + coord.x;
            regionNumber = regionMap[regionMapIndex];
            regionNumberSum += regionNumber;

            if (regionNumber !== 0) {
                coords.push(coord);
                stats.push(regionStats[regionNumber - 1]);
            } else {
                unLabeled.push(coord);
            }
        }

        if (regionNumberSum === adjacentCoords.length * regionNumber) {
            closestRegion = regionNumber;
            closestCoord = adjacentCoords[0];
        } else {
            if (stats.length) {
                for (i = 0; i < stats.length; i++) {
                    coord = coords[i];
                    pixelCoord = getPixelCoordinate(sourceCanvas.width, coord.x, coord.y);
                    regionMapIndex = coord.y * sourceCanvas.width + coord.x;
                    regionNumber = regionMap[regionMapIndex];
                    regionNumberSum += regionNumber;

                    distance = hsvDistance(
                        rgbToHSV(sourceData.data.subarray(pixelCoord, pixelCoord + 3)),
                        calcRGBcolorToHSV(stats[i].mean)
                    );

                    if (
                        distance < smallestDistance ||
                        distance === smallestDistance && stats[i].count >= smallestDistanceStats.count
                    ) {
                        smallestDistance = distance;
                        closestRegion = regionMap[regionMapIndex];
                        smallestDistanceStats = stats[i];
                        closestCoord = coord;
                        closestIndex = 1;
                    }
                }
            }
        }

        return {closestRegion: closestRegion, coord: closestCoord, unLabeled: unLabeled};
    }

    var closestRegionInfo;

    var pixelStatuses = new Uint8Array(sourceCanvas.width * sourceCanvas.height);

    //seeded region growing

    var growTimeout;

    var $growProgress = $('<div>');
    var growStepCount = 0;

    function growRegionsStep() {
        growStepCount++;

        $growProgress.text('Growing regions, pass ' + growStepCount);

        var allDone = true;
        pixelStatuses = new Uint8Array(sourceCanvas.width * sourceCanvas.height);

        for (y = 0; y < sourceCanvas.height; y++) {
            for (x = 0; x < sourceCanvas.width; x++) {
                i = getPixelCoordinate(sourceCanvas.width, x, y);
                regionMapIndex = y * sourceCanvas.width + x;

                if (regionMap[regionMapIndex] === 0 && pixelStatuses[regionMapIndex] === 0) {
                    adjacentCoords = calcAdjacentCoordinates(sourceCanvas.width, sourceCanvas.height, x, y);
                    closestRegionInfo = closestRegion(x, y, adjacentCoords);

                    if (
                        closestRegionInfo.closestRegion !== 0 && closestRegionInfo.coord &&
                        pixelStatuses[closestRegionInfo.coord.y * sourceCanvas.width + closestRegionInfo.coord.x] === 0
                        ) {
                        regionNumber = closestRegionInfo.closestRegion;
                        regionStats[regionNumber - 1].mean = runningColorMean(
                            regionStats[regionNumber - 1].mean,
                            regionStats[regionNumber - 1].count,
                            sourceData.data.subarray(i, i + 3)
                        );
                        regionStats[regionNumber - 1].count++;
                        regionMap[regionMapIndex] = regionNumber;

                        pixelStatuses[regionMapIndex] = 1;
                        //pixelStatuses[closestRegionInfo.coord.y * sourceCanvas.width + closestRegionInfo.coord.x] = 1;

                        allDone = false;
                    }
                }
            }
        }

        //draw();
        //takeSnapShot();

        if (allDone === false) {
            growTimeout = setTimeout(growRegionsStep, 0);
        } else {
            $growProgress.text('Growing regions done');
            draw();
            takeSnapShot();
            mergeRegionsByColor();
        }
    }

    function startGrowingRegions() {
        if (growRegions) {
            $log.append($growProgress);
            growTimeout = setTimeout(growRegionsStep, 0);
        }
    }

    pixelStatuses = new Uint8Array(sourceCanvas.width * sourceCanvas.height);

    function mergeRegions(region1Number, region2Number) {
        var regionMapIndex, x, y;

        regionStats[region1Number - 1].mean = weightedColorMean(
            regionStats[region1Number - 1].mean,
            regionStats[region1Number - 1].count,
            regionStats[region2Number - 1].mean,
            regionStats[region2Number - 1].count
        );
        regionStats[region1Number - 1].count += regionStats[region2Number - 1].count;

        regionStats[region2Number - 1].count = 0;

        for (y = 0; y < sourceCanvas.height; y++) {
            for (x = 0; x < sourceCanvas.width; x++) {
                regionMapIndex = y * sourceCanvas.width + x;
                if (regionMap[regionMapIndex] === region2Number) {
                    regionMap[regionMapIndex] = region1Number;

                    pixelStatuses[regionMapIndex] = 1;
                }
            }
        }
    }

    function mergeRegionsByColor () {
        if (!mergeByColor) {
            return false;
        }

        console.log('mergeRegionsByColor');

        var $mergeProgress = $('<div>');
        $log.append($mergeProgress);

        var n = -1,
            length = regionStats.length;

        async.forEachSeries(regionStats, function (stat, callback) {
            n++;

            $mergeProgress.text('Merging regions by color ' + (n / length * 100).toFixed(1) + '%');
            if (n % 100 === 0) {
                console.log(n);
            }

            var m,
                neighborRegions,
                distance,
                allDone = false;

            if (stat.count !== 0) {
                regionNumber = n + 1;

                async.whilst(
                    function () {
                        return allDone === false;
                    },
                    function (cb) {
                        allDone = true;
                        neighborRegions = getAllNeighboringRegions(regionNumber);

                        m = -1;

                        async.forEachSeries(neighborRegions, function (neighbor, cb2) {
                            m++;

                            distance = hsvDistance(
                                calcRGBcolorToHSV(stat.mean),
                                calcRGBcolorToHSV(regionStats[neighborRegions[m] - 1].mean)
                            );
                            if (distance < colorMergeThreshold) {
                                mergeRegions(regionNumber, neighborRegions[m]);
                                //draw();
                                allDone = false;

                                setTimeout(cb2, stepDelay);
                            } else {
                                cb2();
                            }
                        }, function () {
                            cb();
                        });
                    },
                    function () {
                        if (allDone) {
                            draw();
                        }
                        callback();
                    }
                );
            } else {
                callback();
            }
        }, function () {
            $mergeProgress.text('Merging regions by color done');
            takeSnapShot();
            mergeRegionsBySize();
        });
    }

    function getNeighboringRegions(x, y) {
        var region = regionMap[y * sourceCanvas.width + x],
            regions = [],
            neighborRegion,
            coord,
            coords = calcAdjacentCoordinates(sourceCanvas.width, sourceCanvas.height, x, y),
            i;

        for (i = 0; i < coords.length; i++) {
            coord = coords[i];
            neighborRegion = regionMap[coord.y * sourceCanvas.width + coord.x];
            if (neighborRegion !== region && regions.indexOf(neighborRegion) === -1) {
                regions.push(neighborRegion);
            }
        }

        return regions;
    }

    function getAllNeighboringRegions(regionNumber) {
        var neighborRegions = [],
            newRegions,
            i;

        for (y = 0; y < sourceCanvas.height; y++) {
            for (x = 0; x < sourceCanvas.width; x++) {
                if (regionNumber === regionMap[y * sourceCanvas.width + x]) {
                    newRegions = getNeighboringRegions(x, y);
                    for (i = 0; i < newRegions.length; i++) {
                        if (neighborRegions.indexOf(newRegions[i]) === -1) {
                            neighborRegions.push(newRegions[i]);
                        }
                    }
                }
            }
        }

        return neighborRegions;
    }

    function mergeRegionsBySize () {
        if (!mergeBySize) {
            return false;
        }
        console.log('mergeRegionsBySize');

        var $mergeProgress = $('<div>');
        $log.append($mergeProgress);

        var n = -1,
            length = regionStats.length;

        async.forEachSeries(regionStats, function (stat, callback) {
            n++;

            $mergeProgress.text('Merging regions by size ' + (n / length * 100).toFixed(1) + '%');
            if (n % 100 === 0) {
                console.log(n);
            }

            var m,
                neighborRegions,
                smallestDistance = 10000,
                distance,
                closestRegion,
                wasMerged = false;

            if (stat.count !== 0) {
                regionNumber = n + 1;

                async.whilst(
                    function () {
                        return regionStats[regionNumber - 1].count < sizeMergeThreshold;
                    },
                    function (cb) {
                        neighborRegions = getAllNeighboringRegions(regionNumber);

                        if (neighborRegions.length > 0) {
                            smallestDistance = 10000;
                            for (m = 0; m < neighborRegions.length; m++) {
                                if (regionStats[neighborRegions[m] - 1] === undefined) {
                                    console.log(neighborRegions[m] - 1);
                                }
                                distance = hsvDistance(
                                    calcRGBcolorToHSV(stat.mean),
                                    calcRGBcolorToHSV(regionStats[neighborRegions[m] - 1].mean)
                                );
                                if (distance < smallestDistance) {
                                    smallestDistance = distance;
                                    closestRegion = neighborRegions[m];
                                }
                            }
                            mergeRegions(regionNumber, closestRegion);
                            wasMerged = true;
                            //draw();
                        }

                        setTimeout(cb, stepDelay);
                    },
                    function () {
                        if (wasMerged) {
                            draw();
                        }
                        callback();
                    }
                );
            } else {
                callback();
            }
        }, function () {
            $mergeProgress.text('Merging regions by size done');
            takeSnapShot();
            $log.append('<div>All done</div>');
            console.log('done');
            callback();
        });
    }
}

function runningColorMean(avgColor, count, color) {
    return [
            (avgColor[0] * count + color[0]) / (count + 1),
            (avgColor[1] * count + color[1]) / (count + 1),
            (avgColor[2] * count + color[2]) / (count + 1)
    ]
}

function weightedColorMean(color1, count1, color2, count2) {
    return [
            (color1[0] * count1 + color2[0] * count2) / (count1 + count2),
            (color1[1] * count1 + color2[1] * count2) / (count1 + count2),
            (color1[2] * count1 + color2[2] * count2) / (count1 + count2)
    ]
}

function calcAdjacentCoordinates(width, height, centerX, centerY) {
    var coords = [
            {x: centerX, y: centerY - 1},
            {x: centerX, y: centerY + 1},
            {x: centerX + 1, y: centerY},
            {x: centerX - 1, y: centerY}
        ],
        result = [],
        coord,
        i;

    for (i = 0; i < coords.length; i++) {
        coord = coords[i];
        if (coord.x >= 0 && coord.x < width && coord.y >= 0 && coord.y < height) {
            result.push(coord);
        }
    }

    return result;
}

function valuesStats(values) {
    var mean = 0,
        stdDev = 0,
        i;

    for (i = 0; i < values.length; i++) {
        mean += values[i];
    }

    mean /= values.length;

    for (i = 0; i < values.length; i++) {
        stdDev += Math.pow(values[i] - mean, 2);
    }

    stdDev /= values.length;

    return {mean: mean, stdDev: stdDev};
}

function calcFuzzyEntropyImage(data, imageWidth) {
    var height = data.length / imageWidth / 4,
        x, y, i,
        colors,
        result = new Float32Array(imageWidth * height);

    for (y = 0; y < height; y++) {
        for (x = 0; x < imageWidth; x++) {
            i = y * imageWidth + x;
            colors = rgbaColorsToHsvColors(getSurroundingPixelsData(data, imageWidth, x, y).data);
            result[i] = calcColorsFuzzyEntropy(colors);
        }
    }

    return result;
}

function calcSmoothnessImage(data, imageWidth) {
    var height = data.length / imageWidth / 4,
        x, y, i,
        colors,
        result = new Float32Array(imageWidth * height);

    for (y = 0; y < height; y++) {
        for (x = 0; x < imageWidth; x++) {
            i = y * imageWidth + x;
            colors = rgbaColorsToHsvColors(getSurroundingPixelsData(data, imageWidth, x, y).data);
            result[i] = calcColorsSmoothness(colors);
        }
    }

    return result;
}

function isSeedPixel(gImage, gImageStats, dImage, dImageStats, imageWidth, x, y) {
    //return getNoEdgeThreshold(gImage, gImageStats, imageWidth, x, y);
    //return getSmoothnessThreshold(dImage, dImageStats, imageWidth, x, y);
    return getNoEdgeThreshold(gImage, gImageStats, imageWidth, x, y) && getSmoothnessThreshold(dImage, dImageStats, imageWidth, x, y);
}

function getNoEdgeThreshold(gImage, gImageStats, imageWidth, x, y) {
    var threshold = gImageStats.mean;

    if (gImageStats.mean - 0.5 * gImageStats.stdDev > 0) {
        threshold = gImageStats.mean - 0.5 * gImageStats.stdDev;
    }

    return gImage[y * imageWidth + x] < threshold;
}

function getSmoothnessThreshold(dImage, dImageStats, imageWidth, x, y) {
    var threshold = dImageStats.mean;

    if (dImageStats.mean - 0.1 * dImageStats.stdDev > 0) {
        threshold = dImageStats.mean - 0.1 * dImageStats.stdDev;
    }

    return dImage[y * imageWidth + x] < threshold;
}

function smoothnessStats (colors, meanColor) {
    var distance,
        distances = [],
        stdDev = 0,
        averageDistance = 0,
        i;

    for (i = 0; i < colors.length; i += 3) {
        distance = hsvDistance(colors.subarray(i, i+ 3), meanColor);
        distances.push(distance);
        averageDistance += distance;
    }

    averageDistance /= distances.length;

    for (i = 0; i < distances.length; i++) {
        stdDev += Math.pow(averageDistance - distances[i], 2);
    }

    stdDev = Math.sqrt(stdDev / distances.length);

    return {mean: distance, stdDev: stdDev, distances: distances};
}

function meanHSVcolor(colors) {
    var mean = [0, 0, 0],
        count = colors.length / 3,
        i;

    for (i = 0; i < colors.length; i += 3) {
        mean[0] += colors[i];
        mean[1] += colors[i + 1];
        mean[2] += colors[i + 2];
    }

    mean[0] /= count;
    mean[1] /= count;
    mean[2] /= count;

    return mean;
}

function rgbaColorsToHsvColors(rgbColors) {
    var hsvColors = new Float32Array(rgbColors.length * 0.75),
        i;

    for (i = 0; i < rgbColors.length; i += 4) {
        hsvColors.set(rgbToHSV(rgbColors.subarray(i, i + 3)), i * 0.75);
    }

    return hsvColors;
}

function hsvDistance(color, avgColor) {
    var h = color[0],
        s = color[1],
        v = color[2],
        ha = avgColor[0],
        sa = avgColor[1],
        va = avgColor[2];

    return Math.sqrt(
            Math.pow(v - va, 2) +
            Math.pow(s * Math.cos(h) - sa * Math.cos(ha), 2) +
            Math.pow(s * Math.sin(h) - sa * Math.sin(ha), 2)
    )

}

function colorsEntropyStats(colors, avg, D) {
    var fuzzyEntropies = [],
        fuzzyEntropy,
        stdDev = 0,
        averageFuzzyEntropy = 0,
        i;

    for (i = 0; i < colors.length; i += 3) {
        fuzzyEntropy = calcFuzzyEntropy(colors[i], avg, D);
        fuzzyEntropies.push(fuzzyEntropy);
        averageFuzzyEntropy += fuzzyEntropy;
    }

    averageFuzzyEntropy /= fuzzyEntropies.length;

    for (i = 0; i < fuzzyEntropies.length; i++) {
        stdDev += Math.pow(averageFuzzyEntropy - fuzzyEntropies[i], 2);
    }

    stdDev = Math.sqrt(stdDev / fuzzyEntropies.length);

    return {mean: averageFuzzyEntropy, stdDev: stdDev, fuzzyEntropies: fuzzyEntropies};
}

function calcColorsSmoothness(colors) {
    var stats = smoothnessStats(colors, meanHSVcolor(colors));

    return stats.mean;
}

function calcColorsFuzzyEntropy(colors) {
    var hueStats = colorsHueStats(colors),
        averageHue = hueStats.mean,
        maxHue = hueStats.max,
        minHue = hueStats.min,
        D = Math.max(maxHue - averageHue, averageHue - minHue),
        entropyStats = colorsEntropyStats(colors, averageHue, D);

    return entropyStats.mean;
}

function calcFuzzyEntropy(hue, avg, D) {
    var fuzzyMean = 1 - 0.5 * Math.pow((hue - avg) / D, 2),
        fuzzyEntropy = -fuzzyMean * Math.log(fuzzyMean) - (1 - fuzzyMean) * Math.log(1 - fuzzyMean);

    if (fuzzyMean < 0.5 || fuzzyMean > 1) {
        console.log('invalid fuzzyMean', fuzzyMean);
    }

    if (isNaN(fuzzyEntropy)) {
        fuzzyEntropy = 0;
    }

    return fuzzyEntropy;
}

function colorsHueStats(colors) {
    var mean = 0,
        max = 0,
        min = 2 * Math.PI,
        hue,
        count = 0,
        i;

    for (i = 0; i < colors.length; i += 3) {
        if (colors[i] !== undefined) {
            hue = colors[i];
            mean += hue;
            count++;

            if (hue > max) {
                max = hue;
            }
            if (hue < min) {
                min = hue;
            }
        }

    }

    mean /= count;

    return {mean: mean, min: min, max: max};
}

/*function rgbColorToHSV (rgbColor) {
    return rgbColorToHSV(rgbColor[0], rgbColor[1], rgbColor[2])
}*/

function rgbToHSV(rgbColor) {
    if (rgbColor === undefined) {
        console.log('rgbColor undefined');
    }

    var index = 3 * (Math.round(rgbColor[0]) * 256 * 256 + Math.round(rgbColor[1]) * 256 + Math.round(rgbColor[2]));

    return hsvLUT.subarray(index, index + 3);
}

function calcRGBcolorToHSV(rgb) {
    return calcRGBtoHSV(rgb[0], rgb[1], rgb[2]);
}

function calcRGBtoHSV(r, g, b) {
    var min = Math.min(r, g, b),
        max = Math.max(r, g, b),
        delta = max - min,
        h, s, v;

    v = max / 255;
    if (max !== 0) {
        s = delta / max;
    } else {
        // black
        return [0, 0, 0];
    }

    if (delta == 0) {
        h = 0;
    } else if (r === max) {
        h = ((g - b) / delta) % 6; // between yellow & magenta
    } else if (g === max) {
        h = 2 + (b - r) / delta; // between cyan & yellow
    } else {
        h = 4 + (r - g) / delta; // between magenta & cyan
    }

    h = Math.floor(h * 60); // degrees
    if (h < 0) {
        h += 360;
    }

    h = h / 180 * Math.PI;

    return [h, s, v];
}

function findPos(obj) {
    var curleft = 0, curtop = 0;
    if (obj.offsetParent) {
        do {
            curleft += obj.offsetLeft;
            curtop += obj.offsetTop;
        } while (obj = obj.offsetParent);
        return { x: curleft, y: curtop };
    }
    return undefined;
}

function getSurroundingPixelsData(data, imageWidth, centerX, centerY) {
    var x, y,
        i = 0,
        centerPixelIndex,
        result,
        centerPixelCoord = getPixelCoordinate(imageWidth, centerX, centerY),
        pixelCoord,
        height = data.length / imageWidth / 4,
        minX = centerX - 1,
        minY = centerY - 1,
        maxX = centerX + 1,
        maxY = centerY + 1;

    if (minX < 0) {
        minX = 0;
    } else if (maxX > imageWidth - 1) {
        maxX = imageWidth - 1;
    }
    if (minY < 0) {
        minY = 0;
    } else if (maxY > height - 1) {
        maxY = height - 1;
    }

    result = new Uint8Array((maxX - minX + 1) * (maxY - minY + 1) * 4);

    for (y = minY; y <= maxY; y++) {
        for (x = minX; x <= maxX; x++) {
            pixelCoord = getPixelCoordinate(imageWidth, x, y);
            if (centerPixelCoord === pixelCoord) {
                centerPixelIndex = i;
            }
            result.set(data.subarray(pixelCoord, pixelCoord + 3), i * 4);
            i++;
        }
    }

    return {data: result, centerIndex: centerPixelIndex};
}

function getPixelCoordinate(width, x, y) {
    return 4 * (y * width + x);
}